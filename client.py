# !/usr/bin/env python3
# -*- coding: utf-8 -*-
from urllib.request import urlopen
from time import sleep

url_data = 'http://localhost:8000'

with urlopen(url_data) as resp:
    data = b''
    while True:
        chunk = resp.read(10)
        if not chunk:
            break
        data += chunk
    print(data.decode('utf-8'))
    print(40 * '-')
    print(resp.getheaders())
    print(40 * '-')
    sleep(2)

with urlopen(url_data, data='shutdown'.encode('ascii')) as resp:
    answer = resp.read(1)
    print(answer)

