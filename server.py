# !/usr/bin/env python3
# -*- coding: utf-8 -*-

from http.server import HTTPServer, BaseHTTPRequestHandler
from hashlib import sha1
import _thread


class BaseHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        response_data = [
            'Its fine'
        ]
        response_text = '\n'.join(response_data)
        responce_context = response_text.encode('utf-8')
        responce_lenght = len(responce_context)
        responce_sha1 = sha1(responce_context).hexdigest()
        self.send_response(200, 'OK')
        self.send_header('Content-type', 'text/plain; charset=utf-8')
        self.send_header('Content-length', str(responce_lenght))
        self.send_header('sha1', str(responce_sha1))
        self.end_headers()
        print(self.headers)
        self.wfile.write(responce_context)

    def do_POST(self):
        cmd = self.rfile.read(8).decode('ascii')
        print(cmd)

        if cmd != 'shutdown':
            self.send_error('401', 'Invalid Request')
            return

        def kill_server(server):
            server.shutdown()

        _thread.start_new_thread(kill_server, (self.server,))

        self.send_response(200, 'OK')
        self.end_headers()
        self.wfile.write(b'-')


srv_address = ('', 8000)
http_server = HTTPServer(srv_address, BaseHTTPRequestHandler)
http_server.serve_forever()
